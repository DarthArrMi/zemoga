package com.dartharrmi.zemogamobiletest.managers

import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.IdRes


/**
 * Manager to handle navigation across the app.
 *
 * @author Miguel Arroyo (jmiguelarroyo12@gmail.com).
 */

class NavigationManager {

    companion object {

        fun startActivity(context: Context, targetActivity: Class<*>, extras: Bundle? = null, requestCode: Int = -1, flags: IntArray? = null) {
            val intent = Intent(context, targetActivity)

            if (extras != null) {
                intent.putExtras(extras)
            }

            if (flags != null && flags.isNotEmpty()) {
                for (flag in flags) {
                    intent.addFlags(flag)
                }
            }

            if (requestCode != -1) {
                (context as Activity).startActivityForResult(intent, requestCode)
            } else {
                context.startActivity(intent)
            }
        }

        fun navigateToFragment(@IdRes containerId: Int, activity: Activity?, fragment: Fragment) {
            activity?.let {
                val fragmentManager = activity.fragmentManager
                if (!activity.isFinishing) {
                    fragmentManager.beginTransaction().replace(containerId, fragment).commit()
                }
            }
        }
    }


}