package com.dartharrmi.zemogamobiletest.managers.networking

import com.dartharrmi.zemogamobiletest.BuildConfig
import com.dartharrmi.zemogamobiletest.domain.model.Address
import com.dartharrmi.zemogamobiletest.domain.model.Post
import com.dartharrmi.zemogamobiletest.domain.model.User
import com.dartharrmi.zemogamobiletest.util.Constants
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.concurrent.TimeUnit

/**
 * @author Miguel Arroyo (jmiguelarroyo12@gmail.com).
 */
interface ApiService {

    @GET(Constants.PATH_POST)
    fun getPosts(): Observable<List<Post>>

    @DELETE(Constants.PATH_POST + "{postId}")
    fun deletePost(@Path("postId") postId: Int)

    @GET(Constants.PATH_USER + "/{userId}")
    fun getUser(@Path("userId") userId: Int): Single<User>

    companion object {

        fun create(): ApiService {
            val clientBuilder = OkHttpClient.Builder()
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)

            if (BuildConfig.DEBUG) {
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                clientBuilder.addInterceptor(httpLoggingInterceptor)
            }

            /*
             * Ugly workaround for issues with Kotlin Reflection and Jackson
             */
            val objectMapper = ObjectMapper()
            val simpleModule = SimpleModule()
            simpleModule.addDeserializer(Address::class.java, Address.AddressDeserializer())
            objectMapper.registerModule(simpleModule)

            val retrofit = Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .client(clientBuilder.build())
                    .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()

            return retrofit.create(ApiService::class.java)
        }

    }
}