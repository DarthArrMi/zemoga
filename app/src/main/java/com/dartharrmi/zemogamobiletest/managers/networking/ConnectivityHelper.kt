package com.dartharrmi.zemogamobiletest.managers.networking

import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectivityHelper @Inject constructor(private val context: Context) {

    fun isNetworkAvailable(): Boolean {
        val connnectivityManager: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connnectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnectedOrConnecting
    }
}