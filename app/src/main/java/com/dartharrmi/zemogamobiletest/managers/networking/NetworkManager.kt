package com.dartharrmi.zemogamobiletest.managers.networking

import com.dartharrmi.zemogamobiletest.domain.model.Post
import com.dartharrmi.zemogamobiletest.domain.model.User
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkManager @Inject constructor(private val apiService : ApiService) {

    fun getPost(): Observable<List<Post>> = apiService.getPosts()

    fun deletePost(postId: Int) = apiService.deletePost(postId)

    fun getUser(userId: Int): Single<User> = apiService.getUser(userId)
}