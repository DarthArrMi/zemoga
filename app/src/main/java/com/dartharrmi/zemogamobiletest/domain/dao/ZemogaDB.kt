package com.dartharrmi.zemogamobiletest.domain.dao

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.dartharrmi.zemogamobiletest.domain.model.Post
import com.dartharrmi.zemogamobiletest.domain.model.User

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Database(entities = arrayOf(Post::class, User::class), version = 1)
abstract class ZemogaDB : RoomDatabase() {

    abstract fun postDao(): PostDao

    abstract fun userDao(): UserDao
}