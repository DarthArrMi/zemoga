package com.dartharrmi.zemogamobiletest.domain.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.*

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("userId", "id", "title", "body")
@Entity(tableName = "post")
data class Post(@ColumnInfo(name = "userId") @JsonProperty("userId") var userId: Int,
                @ColumnInfo(name = "id") @JsonProperty("id") @PrimaryKey var id: Int,
                @ColumnInfo(name = "title") @JsonProperty("title") var title: String,
                @ColumnInfo(name = "body") @JsonProperty("body") var body: String,
                @ColumnInfo(name = "isFavorite") @JsonIgnore var isFavorite: Boolean,
                @ColumnInfo(name = "unread") @JsonIgnore var unread: Boolean) : Parcelable {

    @JsonCreator
    @Ignore
    constructor(@JsonProperty("userId") userId: Int,
                @JsonProperty("id") id: Int,
                @JsonProperty("title")title: String,
                @JsonProperty("body") body: String): this(userId, id, title, body, false, false)

    @Ignore
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(userId)
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(body)
        parcel.writeByte(if (isFavorite) 1 else 0)
        parcel.writeByte(if (unread) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Post> {
        override fun createFromParcel(parcel: Parcel): Post {
            return Post(parcel)
        }

        override fun newArray(size: Int): Array<Post?> {
            return arrayOfNulls(size)
        }
    }
}