package com.dartharrmi.zemogamobiletest.domain.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.dartharrmi.zemogamobiletest.domain.model.User
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    fun getAllUsers(): Flowable<List<User>>

    @Query("SELECT * FROM user WHERE id = :id")
    fun getUserById(id: Int): Single<User?>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun saveUser(user: User)
}