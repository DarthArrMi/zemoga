package com.dartharrmi.zemogamobiletest.domain.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.PrimaryKey
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
data class Address(@ColumnInfo(name = "street") /*@JsonProperty("street")*/ var street: String,
                   @ColumnInfo(name = "suite") /*@JsonProperty("suite")*/ var suite: String,
                   @ColumnInfo(name = "city") /*@JsonProperty("city")*/ var city: String,
                   @ColumnInfo(name = "zipcode") /*@JsonProperty("zipcode")*/ @PrimaryKey var zipcode: String,
                   @ColumnInfo(name = "lat") /*@JsonIgnore*/ var lat: Double,
                   @ColumnInfo(name = "lng") /*@JsonIgnore*/ var lng: Double) {

    constructor(): this("", "", "", "", 0.0, 0.0)

    class AddressDeserializer: JsonDeserializer<Address>() {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Address {
            val addressNode: JsonNode = p.codec.readTree(p)
            val geoNode = addressNode.get("geo")

            val address = Address()
            with(addressNode) {
                address.street = addressNode.get("street").asText()
                address.suite = addressNode.get("suite").asText()
                address.city = addressNode.get("city").asText()
                address.zipcode = addressNode.get("zipcode").asText()
                address.lat = geoNode.get("lat").asDouble()
                address.lng = geoNode.get("lng").asDouble()
            }

            return address
        }
    }
}