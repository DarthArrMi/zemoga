package com.dartharrmi.zemogamobiletest.domain.dao

import android.arch.persistence.room.*
import com.dartharrmi.zemogamobiletest.domain.model.Post
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Dao
interface PostDao {

    @Query("SELECT * FROM post")
    fun getAllPosts(): Flowable<List<Post>>

    @Query("SELECT * FROM post WHERE isFavorite = 1")
    fun getFavoritePosts(): Flowable<List<Post>>

    @Query("SELECT count(*) FROM post")
    fun getNumberOfPost(): Flowable<Int>

    @Query("SELECT * FROM post WHERE id = :id")
    fun getPostById(id: String): Single<Post>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun savePost(device: Post)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun savePosts(devices: List<Post>)

    @Delete
    fun deletePost(device: Post)

    @Query("DELETE FROM post")
    fun deleteAll()

    @Update()
    fun updatePost(post: Post): Int
}