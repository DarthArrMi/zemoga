package com.dartharrmi.zemogamobiletest.domain.repository

import android.util.Log
import com.dartharrmi.zemogamobiletest.domain.dao.PostDao
import com.dartharrmi.zemogamobiletest.domain.model.Post
import com.dartharrmi.zemogamobiletest.managers.networking.NetworkManager
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.exceptions.Exceptions
import io.reactivex.internal.operators.completable.CompletableFromAction
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Singleton
class PostRepository @Inject constructor(private val networkManager: NetworkManager, private val postDao: PostDao) {

    private val TAG = PostRepository::class.java.canonicalName

    fun getPost(forceUpdate: Boolean): Observable<List<Post>> {
        return postDao.getNumberOfPost().toObservable().flatMap {
            if (it == 0 || forceUpdate) {
                networkManager.getPost().doOnNext {
                    if (it.isNotEmpty()) {
                        for (i in 0..19) {
                            it.get(i).unread = true
                        }
                        postDao.savePosts(it)
                    }
                }
            } else {
                postDao.getAllPosts().toObservable().doOnNext {
                }.doOnError {
                    Log.e(TAG, "Error retrieving devices from the network", it)
                    Exceptions.propagate(it)
                }
            }
        }
    }

    fun getFavorites(): Observable<List<Post>> {
        return postDao.getFavoritePosts().toObservable().doOnError {
            Log.e(TAG, "Error retrieving devices from the network", it)
            Exceptions.propagate(it)
        }
    }

    fun updatePost(post: Post): Observable<Int> {
        return Observable.create { emitter -> postDao.updatePost(post) }
    }

    fun deletePost(post: Post): Completable {
        return CompletableFromAction({
            postDao.deletePost(post)
            networkManager.deletePost(post.id)
        })
    }

    fun deleteAll(): Completable {
        return CompletableFromAction({
            postDao.deleteAll()
        })
    }
}