package com.dartharrmi.zemogamobiletest.domain.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverter
import android.arch.persistence.room.TypeConverters
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.databind.ObjectMapper

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("id", "name", "username", "email", "address", "phone", "website")
@Entity(tableName = "user")
@TypeConverters(User.AddressConverter::class)
data class User(@JsonProperty("id") @PrimaryKey var id: Int,
                @JsonProperty("name") var name: String,
                @JsonProperty("username") var userName: String,
                @JsonProperty("email") var email: String,
                @JsonProperty("address") var address: Address,
                @JsonProperty("phone") var phone: String,
                @JsonProperty("website") var website: String) {

    class AddressConverter {

        @TypeConverter
        fun toAddress(value: String?): Address {
            value?.let {
                return ObjectMapper().readValue(it, Address::class.java)
            }

            return Address()
        }

        @TypeConverter
        fun fromAddress(address: Address): String {
            return ObjectMapper().writeValueAsString(address)
        }
    }
}