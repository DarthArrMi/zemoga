package com.dartharrmi.zemogamobiletest.domain.repository

import android.util.Log
import com.dartharrmi.zemogamobiletest.domain.dao.UserDao
import com.dartharrmi.zemogamobiletest.domain.model.User
import com.dartharrmi.zemogamobiletest.managers.networking.NetworkManager
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Singleton
class UserRepository @Inject constructor(private val networkManager: NetworkManager, private val userDao: UserDao) {

    fun getUser(userId: Int): Single<User?> {
        return userDao.getUserById(userId).doOnSuccess {
            /*
            If the query succeded, but no user comes in the result, it's because no record was found,
            in this case fetch and save the user
             */
            if (it == null) {
                networkManager.getUser(userId).doOnSuccess {
                    userDao.saveUser(it)
                }.doOnError {
                    it.printStackTrace()
                }
            }
        }.onErrorResumeNext {
            // If an error happened while reading the DB, let's fetch and save the user
            networkManager.getUser(userId).doOnSuccess {
                userDao.saveUser(it)
            }.doOnError {
                it.printStackTrace()
            }
        }
    }
}