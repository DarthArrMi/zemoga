package com.dartharrmi.zemogamobiletest.util

import android.graphics.Canvas
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import com.dartharrmi.zemogamobiletest.presentation.adapter.PostAdapter

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
class SwipeToDeleteItemTouchHelper(dragDirs: Int, swipeDirs: Int, val listener: SwipeToDeleteTouchHelperListener) : ItemTouchHelper.SimpleCallback(dragDirs, swipeDirs) {

    interface SwipeToDeleteTouchHelperListener {
        fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int);
    }

    override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
        return true
    }

    override fun clearView(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?) {
        super.clearView(recyclerView, viewHolder)
        getDefaultUIUtil().clearView((viewHolder as PostAdapter.ViewHolder).mForegroundView);
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        viewHolder?.let {
            val foregroundView = (viewHolder as PostAdapter.ViewHolder).mForegroundView
            ItemTouchHelper.Callback.getDefaultUIUtil().onSelected(foregroundView)
        }
    }

    override fun onChildDrawOver(c: Canvas?, recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        viewHolder?.let {
            val foregroundView = (viewHolder as PostAdapter.ViewHolder).mBackgroundView
            getDefaultUIUtil().clearView(foregroundView);
        }
    }

    override fun onChildDraw(c: Canvas?, recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        viewHolder?.let {
            val foregroundView = (viewHolder as PostAdapter.ViewHolder).mForegroundView
            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive)
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        listener.onSwiped(viewHolder, direction, viewHolder.adapterPosition)
    }
}