package com.dartharrmi.zemogamobiletest.util

/**
 * @author Miguel Arroyo (jmiguelarroyo12@gmail.com).
 */
class Constants {

    companion object {

        // Paths
        const val PATH_POST = "posts"
        const val PATH_USER = "users"
        const val PATH_COMMENTS = "comments"
    }
}