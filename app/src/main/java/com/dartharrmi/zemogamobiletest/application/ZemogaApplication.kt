package com.dartharrmi.zemogamobiletest.application

import android.app.Activity
import android.app.Application
import android.support.multidex.MultiDexApplication
import com.dartharrmi.zemogamobiletest.di.components.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
class ZemogaApplication : MultiDexApplication(), HasActivityInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder().create(this).inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }
}