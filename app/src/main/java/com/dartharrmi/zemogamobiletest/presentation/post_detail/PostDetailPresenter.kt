package com.dartharrmi.zemogamobiletest.presentation.post_detail

import android.util.Log
import com.dartharrmi.zemogamobiletest.domain.model.Post
import com.dartharrmi.zemogamobiletest.domain.repository.PostRepository
import com.dartharrmi.zemogamobiletest.domain.repository.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Singleton
class PostDetailPresenter @Inject constructor(private val postRepository: PostRepository,
                                              private val userRepository: UserRepository) : PostDetailContract.Presenter {

    override val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var mView: PostDetailContract.View? = null

    override fun attachView(view: PostDetailContract.View) {
        mView = view
    }

    override fun detachView() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }

        mView = null
    }

    override fun getComments(forceUpdate: Boolean, postId: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getUserData(forceUpdate: Boolean, userId: Int) {
        compositeDisposable.add(userRepository.getUser(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.let {
                        mView?.showUserData(it)
                    }
                }, {
                    it.printStackTrace()
                }))
    }

    override fun markPostAsRead(post: Post) {
        compositeDisposable.add(postRepository.updatePost(post)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                }, {
                }))
    }
}