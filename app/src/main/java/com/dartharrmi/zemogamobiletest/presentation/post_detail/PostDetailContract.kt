package com.dartharrmi.zemogamobiletest.presentation.post_detail

import com.dartharrmi.zemogamobiletest.base.BaseContract
import com.dartharrmi.zemogamobiletest.domain.model.Post
import com.dartharrmi.zemogamobiletest.domain.model.User
import io.reactivex.disposables.CompositeDisposable

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
interface PostDetailContract {

    interface View : BaseContract.BaseView {

        fun showComments(postId: Int)

        fun showUserData(user: User)
    }

    interface Presenter : BaseContract.BasePresenter<View> {
        val compositeDisposable : CompositeDisposable

        fun getComments(forceUpdate: Boolean, postId: Int)

        fun getUserData(forceUpdate: Boolean, userId: Int)

        fun markPostAsRead(post: Post)
    }
}