package com.dartharrmi.zemogamobiletest.presentation.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.GONE
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dartharrmi.zemogamobiletest.R
import com.dartharrmi.zemogamobiletest.domain.model.Post
import kotlinx.android.synthetic.main.layout_item_post.view.*

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
class PostAdapter(val mContext: Context) : Adapter<PostAdapter.ViewHolder>() {

    interface OnPostClickListener {
        fun onItemClick(view: View?, post: Post)

        fun onItemStarred(position: Int, post: Post)
    }

    var postClickListener: OnPostClickListener? = null

    var mPostItems: MutableList<Post> = mutableListOf()

    fun addPosts(posts: List<Post>) {
        mPostItems.addAll(posts)
        notifyDataSetChanged()
    }

    fun removePost(position: Int) {
        mPostItems.removeAt(position)
        notifyItemRemoved(position)
    }

    fun removeAll() {
        val originalSize = mPostItems.size
        mPostItems.clear()
        notifyItemRangeRemoved(0, originalSize)
    }

    fun insertPost(post: Post, position: Int) {
        mPostItems.add(position, post)
        notifyItemInserted(position)
    }

    fun getPost(position: Int): Post {
        return mPostItems.get(position)
    }

    fun startPost(position: Int){
        mPostItems.get(position).isFavorite = true
        notifyItemChanged(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_item_post, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mPostItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentPost = mPostItems[position]

        holder.mPostTitle.setText(currentPost.title)
        holder.mPostBody.setText(currentPost.body)
        when (currentPost.unread) {
            true -> holder.mPostUnreadIndicator.visibility = VISIBLE
        }
        when (currentPost.isFavorite) {
            true -> holder.mPostFavorite.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_star_filled))
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var mPostTitle: TextView
        lateinit var mPostBody: TextView
        lateinit var mPostUnreadIndicator: ImageView
        lateinit var mPostFavorite: ImageView
        lateinit var mForegroundView: View
        lateinit var mBackgroundView: View

        init {
            with(itemView) {
                mPostTitle = this.postTextTitle
                mPostBody = this.postTextBody
                mPostUnreadIndicator = this.postImageUnreadIndicator
                mPostFavorite = itemView.postImageFavorite
                mForegroundView = itemView.viewForeground
                mBackgroundView = itemView.viewBackground

                this.setOnClickListener {
                    val currentPost = getPost(adapterPosition)
                    currentPost.unread = false
                    postClickListener?.onItemClick(it, currentPost)
                }

                mPostFavorite.setOnClickListener {
                    val currentPost = getPost(adapterPosition)
                    postClickListener?.onItemStarred(adapterPosition, currentPost)
                }
            }
        }
    }
}