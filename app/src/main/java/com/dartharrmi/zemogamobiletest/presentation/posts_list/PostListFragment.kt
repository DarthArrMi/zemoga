package com.dartharrmi.zemogamobiletest.presentation.posts_list

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.LinearLayout
import com.dartharrmi.zemogamobiletest.R
import com.dartharrmi.zemogamobiletest.domain.model.Post
import com.dartharrmi.zemogamobiletest.managers.NavigationManager
import com.dartharrmi.zemogamobiletest.presentation.adapter.PostAdapter
import com.dartharrmi.zemogamobiletest.presentation.post_detail.PostDetailActivity
import com.dartharrmi.zemogamobiletest.presentation.posts_list.PostOperationsContract.Presenter
import com.dartharrmi.zemogamobiletest.util.SwipeToDeleteItemTouchHelper
import com.dartharrmi.zemogamobiletest.util.SwipeToDeleteItemTouchHelper.SwipeToDeleteTouchHelperListener
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_post_list.*
import javax.inject.Inject


/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
class PostListFragment : Fragment(), PostOperationsContract.View, SwipeToDeleteTouchHelperListener {

    companion object {
        fun newInstance(): PostListFragment {
            val postListFragment = PostListFragment()
            return postListFragment
        }
    }

    @Inject
    lateinit var mPresenter: Presenter

    lateinit var mPostAdapter: PostAdapter

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        context?.let {
            mPostAdapter = PostAdapter(it)
        }
        mPresenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_post_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dashboardRecyclerView.adapter = mPostAdapter
        dashboardRecyclerView.layoutManager = LinearLayoutManager(context)
        dashboardRecyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        dashboardRecyclerView.itemAnimator = DefaultItemAnimator()

        val simpleCallback: ItemTouchHelper.SimpleCallback = SwipeToDeleteItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(simpleCallback).attachToRecyclerView(dashboardRecyclerView)

        mPostAdapter.postClickListener = object : PostAdapter.OnPostClickListener {
            override fun onItemStarred(position: Int, post: Post) {
                mPresenter.starPost(position, post)
            }

            override fun onItemClick(view: View?, post: Post) {
                showPostDetails(post)
            }
        }

        dashboardSwipeToRefresh.setOnRefreshListener {
            dashboardSwipeToRefresh.isRefreshing = true
            mPresenter.getDevices(true)
        }

        mPresenter.getDevices(false)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        val deletedItem = mPostAdapter.getPost(viewHolder.adapterPosition)
        val deletedIndex = viewHolder.adapterPosition

        mPostAdapter.removePost(deletedIndex)
        // showing snack bar with Undo option
        Snackbar
                .make(dashboardRecyclerView, "Post was removed", Snackbar.LENGTH_LONG)
                .setAction("UNDO") {
                    // undo is selected, restore the deleted item
                    mPostAdapter.insertPost(deletedItem, deletedIndex)
                }.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        mPresenter.deletePost(deletedItem)
                    }
                }).show()
    }

    fun deleteAll() {
        mPostAdapter.removeAll()
    }

    //region View Implementation
    override fun showPostList(posts: List<Post>) {
        dashboardSwipeToRefresh.isRefreshing = false
        mPostAdapter.addPosts(posts)
    }

    override fun showPostDetails(post: Post) {
        val extras = Bundle()
        extras.putParcelable(PostDetailActivity.EXTRA_POST, post)

        context?.let {
            NavigationManager.startActivity(it, PostDetailActivity::class.java, extras)
        }
    }

    override fun showLoadingMessage(loadingMessage: String) {
        dashboardProgressBar.visibility = VISIBLE
    }

    override fun hideLoadingMessage() {
        dashboardProgressBar.visibility = GONE
    }

    override fun showErrorMessage(errorMessage: String) {
        Snackbar.make(this.view!!, "An error happened while getting your posts", Snackbar.LENGTH_LONG).show()
    }

    override fun starPost(position: Int) {
        mPostAdapter.startPost(position)
    }

    override fun clearList() {
        mPostAdapter.removeAll()
    }
    //endregion
}