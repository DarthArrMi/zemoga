package com.dartharrmi.zemogamobiletest.presentation.post_detail

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import com.dartharrmi.zemogamobiletest.R
import com.dartharrmi.zemogamobiletest.domain.model.Post
import com.dartharrmi.zemogamobiletest.domain.model.User
import com.dartharrmi.zemogamobiletest.presentation.post_detail.PostDetailContract.View
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_post_detail.*
import javax.inject.Inject

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
class PostDetailActivity: AppCompatActivity(), View {

    companion object {
        val EXTRA_POST = "EXTRA_POST"
    }

    @Inject
    lateinit var mDetailPresenter: PostDetailPresenter

    lateinit var mPost: Post

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_detail)

        mPost = intent.extras.getParcelable(EXTRA_POST)
        mPost.unread = false

        postDetailTitle.setText(mPost.title)
        postDetailBody.setText(mPost.body)

        mDetailPresenter.attachView(this)
        mDetailPresenter.getUserData(false, mPost.userId)
    }

    override fun onResume() {
        super.onResume()

        mDetailPresenter.markPostAsRead(mPost)
    }

    override fun showComments(postId: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showUserData(user: User) {
        postDetailUserName.setText(user.name)
        postDetailNick.setText(user.userName)
        postDetailEmail.setText(user.email)
        postDetailPhone.setText(user.phone)
        postDetailWebsite.setText(user.website)
    }

    override fun showLoadingMessage(loadingMessage: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideLoadingMessage() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showErrorMessage(errorMessage: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}