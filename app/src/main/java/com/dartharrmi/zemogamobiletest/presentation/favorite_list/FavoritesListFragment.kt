package com.dartharrmi.zemogamobiletest.presentation.favorite_list

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.LinearLayout
import com.dartharrmi.zemogamobiletest.R
import com.dartharrmi.zemogamobiletest.domain.model.Post
import com.dartharrmi.zemogamobiletest.presentation.favorite_list.FavoritesOperationsContract.Presenter
import com.dartharrmi.zemogamobiletest.presentation.adapter.PostAdapter
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_post_list.*
import javax.inject.Inject

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
class FavoritesListFragment : Fragment(), FavoritesOperationsContract.View {
    companion object {
        val ARG_TYPE = "ARG_TYPE"
        fun newInstance(): FavoritesListFragment {
            val postListFragment = FavoritesListFragment()
            return postListFragment
        }
    }

    @Inject
    lateinit var mPresenter: Presenter

    lateinit var mPostAdapter: PostAdapter

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        context?.let {
            mPostAdapter = PostAdapter(it)
        }
        mPresenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_post_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dashboardRecyclerView.adapter = mPostAdapter
        dashboardRecyclerView.layoutManager = LinearLayoutManager(context)
        dashboardRecyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))

        dashboardSwipeToRefresh.isRefreshing = false
        dashboardSwipeToRefresh.isEnabled = false

        mPresenter.getFavorites()
    }

    //region View Implementation
    override fun showPostList(posts: List<Post>) {
        mPostAdapter.addPosts(posts)
    }

    override fun showLoadingMessage(loadingMessage: String) {
        dashboardProgressBar.visibility = VISIBLE
    }

    override fun hideLoadingMessage() {
        dashboardProgressBar.visibility = GONE
    }

    override fun showErrorMessage(errorMessage: String) {
        Snackbar.make(this.view!!, "An error happened while getting your posts", Snackbar.LENGTH_LONG).show()
    }
    //endregion
}