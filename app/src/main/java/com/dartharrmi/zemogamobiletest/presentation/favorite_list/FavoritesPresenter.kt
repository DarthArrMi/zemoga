package com.dartharrmi.zemogamobiletest.presentation.favorite_list

import com.dartharrmi.zemogamobiletest.domain.repository.PostRepository
import com.dartharrmi.zemogamobiletest.presentation.favorite_list.FavoritesOperationsContract.Presenter
import com.dartharrmi.zemogamobiletest.presentation.favorite_list.FavoritesOperationsContract.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Singleton
class FavoritesPresenter @Inject constructor(private val postRepository: PostRepository) : Presenter {

    override val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var mView: View? = null

    override fun attachView(view: View) {
        mView = view
    }

    override fun getFavorites() {
        compositeDisposable.addAll(postRepository.getFavorites()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    mView?.showPostList(it)
                }, {
                    mView?.hideLoadingMessage()
                    mView?.showErrorMessage("dsfsdf")
                }))
    }

    override fun detachView() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }

        mView = null
    }
}