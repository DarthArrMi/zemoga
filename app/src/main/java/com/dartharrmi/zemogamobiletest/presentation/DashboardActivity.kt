package com.dartharrmi.zemogamobiletest.presentation

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.dartharrmi.zemogamobiletest.R
import com.dartharrmi.zemogamobiletest.presentation.favorite_list.FavoritesListFragment
import com.dartharrmi.zemogamobiletest.presentation.posts_list.PostListFragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_dashboard.*
import java.util.*
import javax.inject.Inject

class DashboardActivity : AppCompatActivity(), HasSupportFragmentInjector {
    @Inject
    lateinit var mFragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return mFragmentDispatchingAndroidInjector
    }

    private lateinit var mAdapter: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(toolbar)

        val fragments = ArrayList<Fragment>()
        fragments.add(PostListFragment.newInstance())
        fragments.add(FavoritesListFragment.newInstance())

        mAdapter = ViewPagerAdapter(supportFragmentManager, fragments, resources.getStringArray(R.array.categories_title_list).toList())
        coupon_list_view_pager.adapter = mAdapter
        toolbarTab.setupWithViewPager(coupon_list_view_pager)
        toolbarTab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    1 -> {
                        fab.visibility = View.GONE
                    }
                }
            }
        })

        fab.setOnClickListener { view ->
            (fragments.get(0) as PostListFragment).deleteAll()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_dashboard, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private inner class ViewPagerAdapter internal constructor(fm: FragmentManager, private val mFragments: MutableList<Fragment>, private val mFragmentTitleList: List<String>) : FragmentPagerAdapter(fm) {

        init {
            if (mFragments.size != mFragmentTitleList.size) {
                throw IllegalArgumentException("mFragments and mFragmentTitleList must have the same length")
            }
        }

        override fun getItem(position: Int): Fragment {
            return mFragments[position]
        }

        override fun getCount(): Int {
            return mFragments.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }
}
