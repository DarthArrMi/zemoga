package com.dartharrmi.zemogamobiletest.presentation.favorite_list

import com.dartharrmi.zemogamobiletest.base.BaseContract.BasePresenter
import com.dartharrmi.zemogamobiletest.base.BaseContract.BaseView
import com.dartharrmi.zemogamobiletest.domain.model.Post
import io.reactivex.disposables.CompositeDisposable

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
interface FavoritesOperationsContract {

    interface View : BaseView {
        fun showPostList(posts: List<Post>)
    }

    interface Presenter : BasePresenter<View> {
        val compositeDisposable : CompositeDisposable

        fun getFavorites()
    }
}