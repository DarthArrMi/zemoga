package com.dartharrmi.zemogamobiletest.presentation.posts_list

import com.dartharrmi.zemogamobiletest.base.BaseContract.BasePresenter
import com.dartharrmi.zemogamobiletest.base.BaseContract.BaseView
import com.dartharrmi.zemogamobiletest.domain.model.Post
import io.reactivex.disposables.CompositeDisposable

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
interface PostOperationsContract {

    interface View : BaseView {
        fun showPostList(posts: List<Post>)

        fun clearList()

        fun showPostDetails(post: Post)

        fun starPost(position: Int)
    }

    interface Presenter : BasePresenter<View> {
        val compositeDisposable : CompositeDisposable

        fun getDevices(forceUpdate: Boolean)

        fun deletePost(post: Post)

        fun deleteAllPost()

        fun starPost(position: Int, post: Post)
    }
}