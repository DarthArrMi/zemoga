package com.dartharrmi.zemogamobiletest.presentation.posts_list

import android.util.Log
import com.dartharrmi.zemogamobiletest.domain.model.Post
import com.dartharrmi.zemogamobiletest.domain.repository.PostRepository
import com.dartharrmi.zemogamobiletest.presentation.posts_list.PostOperationsContract.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Singleton
class PostsOperationsPresenter @Inject constructor(private val postRepository: PostRepository) : PostOperationsContract.Presenter {

    override val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var mView: View? = null

    override fun attachView(view: View) {
        mView = view
    }

    override fun getDevices(forceUpdate: Boolean) {
        compositeDisposable.add(postRepository.getPost(forceUpdate)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    mView?.hideLoadingMessage()
                    mView?.showPostList(it)
                }, {
                    mView?.hideLoadingMessage()
                    mView?.showErrorMessage("dsfsdf")
                }))
    }

    override fun deletePost(post: Post) {
        compositeDisposable.add(postRepository.deletePost(post)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                }, {
                    mView?.showErrorMessage("dsfsdf")
                }))
    }

    override fun starPost(position: Int, post: Post) {
        post.isFavorite = !post.isFavorite

        compositeDisposable.add(postRepository.updatePost(post)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (it > 0) {
                        mView?.starPost(position)
                    }
                }, {
                    it.printStackTrace()
                })
        )
    }

    override fun deleteAllPost() {
        compositeDisposable.add(postRepository.deleteAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                }, {
                    mView?.showErrorMessage("dsfsdf")
                }))
    }

    override fun detachView() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }

        mView = null
    }
}