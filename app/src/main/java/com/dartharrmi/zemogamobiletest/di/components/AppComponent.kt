package com.dartharrmi.zemogamobiletest.di.components

import com.dartharrmi.zemogamobiletest.application.ZemogaApplication
import com.dartharrmi.zemogamobiletest.di.builders.ActivityBuilder
import com.dartharrmi.zemogamobiletest.di.builders.FragmentBuilder
import com.dartharrmi.zemogamobiletest.di.modules.ActivityModule
import com.dartharrmi.zemogamobiletest.di.modules.ApplicationModule
import com.dartharrmi.zemogamobiletest.di.modules.FragmentsModule
import com.dartharrmi.zemogamobiletest.di.modules.NetworkModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class, ApplicationModule::class, ActivityModule::class,
        FragmentsModule::class, NetworkModule::class, ActivityBuilder::class, FragmentBuilder::class))
interface AppComponent : AndroidInjector<ZemogaApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<ZemogaApplication>()

    override fun inject(instance: ZemogaApplication)
}