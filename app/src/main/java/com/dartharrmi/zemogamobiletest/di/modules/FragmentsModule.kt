package com.dartharrmi.zemogamobiletest.di.modules

import com.dartharrmi.zemogamobiletest.presentation.favorite_list.FavoritesPresenter
import com.dartharrmi.zemogamobiletest.presentation.favorite_list.FavoritesOperationsContract
import com.dartharrmi.zemogamobiletest.presentation.posts_list.PostsOperationsPresenter
import com.dartharrmi.zemogamobiletest.presentation.posts_list.PostOperationsContract
import dagger.Module
import dagger.Provides

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Module
class FragmentsModule {

    @Provides
    fun providesPostPresenter(postsPresenter: PostsOperationsPresenter): PostOperationsContract.Presenter = postsPresenter

    @Provides
    fun providesFavoritesPresenter(reportPresenter: FavoritesPresenter): FavoritesOperationsContract.Presenter = reportPresenter
}