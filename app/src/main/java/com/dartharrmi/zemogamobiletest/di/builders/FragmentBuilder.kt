package com.dartharrmi.zemogamobiletest.di.builders

import com.dartharrmi.zemogamobiletest.di.modules.FragmentsModule
import com.dartharrmi.zemogamobiletest.presentation.favorite_list.FavoritesListFragment
import com.dartharrmi.zemogamobiletest.presentation.posts_list.PostListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = arrayOf(FragmentsModule::class))
    internal abstract fun bindPostListFragment(): PostListFragment

    @ContributesAndroidInjector(modules = arrayOf(FragmentsModule::class))
    internal abstract fun bindFavoritesListFragment(): FavoritesListFragment
}