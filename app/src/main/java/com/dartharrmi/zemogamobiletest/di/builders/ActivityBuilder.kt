package com.dartharrmi.zemogamobiletest.di.builders

import com.dartharrmi.zemogamobiletest.di.modules.ActivityModule
import com.dartharrmi.zemogamobiletest.presentation.DashboardActivity
import com.dartharrmi.zemogamobiletest.presentation.post_detail.PostDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = arrayOf(ActivityModule::class))
    internal abstract fun bindDashboardActivity(): DashboardActivity

    @ContributesAndroidInjector(modules = arrayOf(ActivityModule::class))
    internal abstract fun bindPostDetailActivity(): PostDetailActivity
}