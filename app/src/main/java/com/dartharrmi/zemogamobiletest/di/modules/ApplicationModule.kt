package com.dartharrmi.zemogamobiletest.di.modules

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.dartharrmi.zemogamobiletest.application.ZemogaApplication
import com.dartharrmi.zemogamobiletest.domain.dao.ZemogaDB
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun providesApplication(application: ZemogaApplication): Application = application

    @Provides
    @Singleton
    fun providesContext(application: ZemogaApplication): Context = application.applicationContext

    @Provides
    @Singleton
    fun providesLocalStorage(application: ZemogaApplication): ZemogaDB {
        return Room.databaseBuilder(application, ZemogaDB::class.java, "ZemogaDB").fallbackToDestructiveMigration().build()
    }

    @Provides
    @Singleton
    fun providesPostDao(zemogaDB: ZemogaDB) = zemogaDB.postDao()

    @Provides
    @Singleton
    fun providesUserDao(zemogaDB: ZemogaDB) = zemogaDB.userDao()
}