package com.dartharrmi.zemogamobiletest.di.modules

import com.dartharrmi.zemogamobiletest.managers.networking.ApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @@author Miguel Arroyo (jmiguelarroyo12@gmail.com)
 */
@Module
class NetworkModule {

    @Provides
    @Singleton
    fun providesApiService() = ApiService.create()
}