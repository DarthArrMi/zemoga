package com.dartharrmi.zemogamobiletest.base

/**
 * Base contract between the view and the mPresenter.
 *
 * @author Miguel Arroyo (jmiguelarroyo12@gmail.com).
 */
interface BaseContract {

    interface BaseView {

        fun showLoadingMessage(loadingMessage : String)

        fun hideLoadingMessage()

        fun showErrorMessage(errorMessage : String)
    }

    interface BasePresenter<in T : BaseView> {

        fun attachView(view: T)

        fun detachView()
    }
}